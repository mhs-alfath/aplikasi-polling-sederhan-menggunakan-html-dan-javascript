var soal = '[{"soal":"Tanggal berapa Indonesia Merdeka?", "jawaban":["17 Agustus 1945", "18 Agustus 1945", "10 Oktober 1950", "30 September 1965"], "pair":"SUkxMTExMTExMTExMDA="},'
			+'{"soal":"Siapa Presiden Pertama Indonesia?", "jawaban":["Mohammad Hatta", "Jendral Sudirman", "Soekarno", "Soeharto"], "pair":"SUkxMTExMTExMTExMTI="},'
			+'{"soal":"Siapa Saja Yang Menyertai Soekarno Saat Dibawa Ke Rengasdengklok oleh PETA?", "jawaban":["Mohammad Hatta, Megawati, Guntur", "Mohammad Hatta, Fatmawati, Guntur", "Achamad Subandrio, Fatmawati, Mohammat Hatta", "Moch Yamin, Megawati, Fatmawati"], "pair":"SUkxMTExMTExMTExMjE="},'
			+'{"soal":"Dimanakah Letak Rumah Penyiapan Teks Proklamasi (Nama Saat ini)?", "jawaban":["Monas", "Jalan Teungku Umar No. 1", "Jalan Proklamasi No. 1", "Jalan Imam Bonjol No. 1"], "pair":"SUkxMTExMTExMTExMzM="},'
			+'{"soal":"Siapa Pengetik Teks Proklamasi?", "jawaban":["Soekarno", "Sayuti Melik", "Sukarni", "Soediro"], "pair":"SUkxMTExMTExMTExNDE="},'
			+'{"soal":"Kapan PPKI Mengesahkan dan Menetapkan UUD?", "jawaban":["18 Agustus 1945", "17 Agustus 1945", "19 Agustus 1950", "11 Desember 1949"], "pair":"SUkxMTExMTExMTExNTA="},'
			+'{"soal":"Tanggal Berapa Hari Pahlawan di Indonesia?", "jawaban":["17 Agustus", "10 November", "5 Oktober", "1 Mei"], "pair":"SUkxMTExMTExMTExNjE="},'
			+'{"soal":"Siapakah Diantara Toko Berikut Yang Tidak Bergelar Jendral Besar?", "jawaban":["Soeharto", "Jend. Sudirman", "Soekarno", "A.H Nasution"], "pair":"SUkxMTExMTExMTExNzI="},'
			+'{"soal":"Kapan Era Reformasi Indonesia di mulai?", "jawaban":["Akhir Tahun 1997", "Akhir Tahun 1999", "Awal Tahun 2000", "Pertengahan 1998"], "pair":"SUkxMTExMTExMTExODM="},'
			+'{"soal":"Siapa Presiden Pertama Yang Dipilih Langsung Oleh Rakyat?", "jawaban":["BJ Habibi", "Gusdur", "Susilo Bambang Yudhoyono", "Megawati"], "pair":"SUkxMTExMTExMTExOTI="}'
			+']';
var awscount = 0;
var strpass = '';
var awsvalue = [];
var fiveMinutes = 10;
var x;
function startQuiz()
{
	//validate fields
	var fail = false;
	var fail_log = '';
	var name;
	$( '#startquiz' ).find( 'select, textarea, input' ).each(function(){
		if( ! $( this ).prop( 'required' )){

		} else {
			if ( ! $( this ).val() ) {
				fail = true;
				alt = $( this ).attr( 'alt' );
				name = $( this ).attr( 'name' );
				fail_log += alt + " harus diisi \n";				
			}

		}
	});

	/* var inputEmail = document.startquiz.email;
	if (inputEmail != ''){
		var mailformat = /^w+([.-]?w+)*@w+([.-]?w+)*(.w{2,3})+$/;
		if(inputEmail.value.match(mailformat))
		{
		}else{
			fail = true;
			fail_log += "Format email salah \n";
		}
	} */
	
	//submit if fail never got set to true
	if ( ! fail ) {
		
		strpass += "II";
		$('#regQuiz').css("display", "none");
		$('#QuizSection').css("display", "");
		awscount++
		var obj = jQuery.parseJSON( soal );
		
        $('#label_soal').text(obj[awscount-1].soal);
        $('#jawaban1').text(obj[awscount-1].jawaban[0]);
        $('#jawaban2').text(obj[awscount-1].jawaban[1]);
        $('#jawaban3').text(obj[awscount-1].jawaban[2]);
        $('#jawaban4').text(obj[awscount-1].jawaban[3]);
		
		display = $('#countdown');
		startTimer(fiveMinutes, display, awscount);
		
	} else {
		alert( fail_log );
	}
}

function answering(valans){
	if (awsvalue[awscount-1] != 99){
		awsvalue[awscount-1] = valans;
		clearInterval(x);
		startQuizNext();
	}
}

function startQuizNext()
{
	var obj = jQuery.parseJSON( soal );
	
	$('#buttonnext').css("display","none");
	$('#jawaban1').css("cursor","pointer");
	$('#jawaban2').css("cursor","pointer");
	$('#jawaban3').css("cursor","pointer");
	$('#jawaban4').css("cursor","pointer");
			
	awscount++
	strpass += "1";
	if (awscount <= 10)
	{
		
		$('#label_soal').text(obj[awscount-1].soal);
		$('#jawaban1').text(obj[awscount-1].jawaban[0]);
		$('#jawaban2').text(obj[awscount-1].jawaban[1]);
		$('#jawaban3').text(obj[awscount-1].jawaban[2]);
		$('#jawaban4').text(obj[awscount-1].jawaban[3]);
		
		display = $('#countdown');
		startTimer(fiveMinutes, display, awscount);
	}
	else{
		//menampilkan hasil
		
		var nilai = 0;
		for(var a=0; a<10; a++)
		{
			var tck = strpass;
			tck += a+""+awsvalue[a];
			var tck_encode = Base64.encode(tck);
			if (tck_encode == obj[a].pair)
			{
				var added = 10;
				nilai += added;
			}
		}
		
		$('#resultvalue').text(nilai);
		$('#QuizSection').css("display","none");
		$('#ResultSection').css("display","");
	}
}

function startTimer(duration, display, awscount) {
	display.css("display","");
	$('#countdown_tmp').css("display","none");
    var timer = duration, minutes, seconds;
	x = setInterval(function () {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.text(minutes + ":" + seconds);

        if (--timer < 0) {
            //timer = duration;
			$('#buttonnext').css("display","");
			$('#jawaban1').css("cursor","not-allowed");
			$('#jawaban2').css("cursor","not-allowed");
			$('#jawaban3').css("cursor","not-allowed");
			$('#jawaban4').css("cursor","not-allowed");
			
			display.css("display","none");
			$('#countdown_tmp').css("display","");
			awsvalue[awscount-1] = 99;
			clearInterval(x);
        }
    }, 1000);
}


$(function() {
		$('#buttonnext').css("display","none");
		$('#regQuiz').css("display","");
		
})	
	
	
	
	